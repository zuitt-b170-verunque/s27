const http = require("http");
const port = 4000;

http.createServer((req,res)=>{
    if (req.url === "/"){
        res.writeHead(200, {"Content-Type":"text/plain"})
        res.end("Welcome to the Booking System")
    }

    if (req.url === "/profile"){
        res.writeHead(200, {"Content-Type":"text/plain"})
        res.end("Welcome to your profile")
    }

    if (req.url === "/courses"){
        res.writeHead(200, {"Content-Type":"text/plain"})
        res.end("Here's our courses available")
    }

    if (req.url === "/addcourse"  && req.method === "POST"){
        res.writeHead(200, {"Content-Type":"text/plain"})
        res.end("Add Course to our resources")
    }

    if (req.url === "/updatecourse"  && req.method === "PUT"){
        res.writeHead(200, {"Content-Type":"text/plain"})
        res.end("Update course to our resources")
    }

    if (req.url === "/archivecourse"  && req.method === "DELETE"){
        res.writeHead(200, {"Content-Type":"text/plain"})
        res.end("Archive courses to our resources")
    }
}
).listen(port);
console.log(`Server is now running at localhost: ${port}`);
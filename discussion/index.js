const http = require ("http");

const port = 4000;

const server = http.createServer((req, res) => {
    if (req.url === "/items" && req.method === "GET") {
        res.writeHead(200, {"Content-Type":"text/plain"});
        res.end("Data retrieved from the database")
    }

    /* 
    create a "items" url with POST method 
    the response should be "Data sent to the database" with 200 as status code and plain textt as teh content type
    */
    if (req.url === "/items" && req.method === "POST" ) {
        
        res.writeHead(200, {"Content-Type":"text/plain"});
        res.end("Data POSTED to the database")
    }
    if (req.url === "/items" && req.method === "PUT" ) {
        
        res.writeHead(200, {"Content-Type":"text/plain"});
        res.end("Data PUT to the database")
    }
    if (req.url === "/items" && req.method === "DELETE" ) {
        
        res.writeHead(200, {"Content-Type":"text/plain"});
        res.end("Data DELETED to the database")
    }
}
)

server.listen(port);
console.log(`Server is now running at localhost: ${port}`);
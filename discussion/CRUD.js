const http = require ("http");


// Mock Database
let database = [
    {
        "name" : "Brandon",
        "email" : "brandon@mail.com"
    },
    {
        "name" : "Jober",
        "email" : "jobert@mail.com"
    }
]

http.createServer((req,res) => {
 
    if(req.url === "/users" && req.method === "GET") {
        res.writeHead(200, {"Content-Type" : "_application/json"});
        res.write(JSON.stringify(database));
        res.end();
    }

    if (req.url=== "/users" && req.method === "POST") {
        let requestBody = ""
        req.on("data", function(data){
            requestBody += data
            console.log(requestBody)
        })

        req.on("end", function(){
            requestBody = JSON.parse(requestBody)

            let newUser = {
                "name" : requestBody.name,
                "email" : requestBody.email
            }
            database.push(newUser);
            // if we are adding to an array we use push
            // if we are adding to an object we use save
            console.log(database);
            
            res.writeHead(200, {"Content-Type" : "_application/json"});
            // the client need string data type for easier readability, that is why we should use JSON.stringify
            res.write(JSON.stringify(newUser));
            res.end();

        })
    }
} 
).listen(3000)

console.log(`Server is now running`) 


